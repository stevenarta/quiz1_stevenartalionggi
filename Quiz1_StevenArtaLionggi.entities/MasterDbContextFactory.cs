﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quiz1_StevenArtaLionggi.entities
{
    public class MasterDbContextFactory : IDesignTimeDbContextFactory<MasterDbContext>
    {
        public MasterDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<MasterDbContext>();
            builder.UseSqlite("Data Source = reference.db");
            return new MasterDbContext(builder.Options);
        }
    }
}
