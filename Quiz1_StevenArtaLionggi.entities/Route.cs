﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Quiz1_StevenArtaLionggi.entities
{
    public class Route
    {
        [Key]
        public int RouteId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Qty { get; set; }

    }
}
