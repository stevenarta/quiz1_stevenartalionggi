﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quiz1_StevenArtaLionggi.entities
{
    public class MasterDbContext:DbContext
    {
        public MasterDbContext(DbContextOptions<MasterDbContext> options):base(options)
        {
        }

        public DbSet<Route> Routes{ set; get; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Route>().ToTable("route");
        }
    }
}
