using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace Quiz1_StevenArtaLionggi
{
    public class Program
    {
        public static int Main(string[] args)
        {
            // The add process is based on the order priority.
            // The later you add the configuration, it will overwrite the other value from previous added configurations.
            // Example: appsettings.json is added later after appsettings.Development.json, so some values in appsettings.Development.json
            // will be overwrite with the values in appsettings.json.
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.Development.json")
                .AddJsonFile("appsettings.Staging.json", optional: true)
                .AddJsonFile("appsettings.json")
                // Will read your secrets.json.
                .AddUserSecrets<Startup>(optional: true)
                .AddEnvironmentVariables()
                .Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            // Without Serilog.Settings.Configuration package....
            //.MinimumLevel.Debug()
            //.MinimumLevel.Override("Microsoft", LogEventLevel.Information)
            //.Enrich.FromLogContext()
            //.WriteTo.Console()
            //.WriteTo.File("/blobs/log.txt", rollingInterval: RollingInterval.Day)
            //.CreateLogger();

            try
            {
                Log.Information("Starting web host");
                Log.Debug("Preparing application....");
                CreateHostBuilder(args).Build().Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .UseSerilog();
    }
}
