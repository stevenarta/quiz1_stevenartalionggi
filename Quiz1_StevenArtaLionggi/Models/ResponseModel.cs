﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_StevenArtaLionggi.Models
{
    public class ResponseModel
    {
        /// <summary>
        /// Attribut berupa string sebagai return value pada service
        /// </summary>
        public string ResponseMessage { get; set; }

    }
}
