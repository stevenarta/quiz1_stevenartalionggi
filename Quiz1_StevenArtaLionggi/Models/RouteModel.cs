﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_StevenArtaLionggi.Models
{
    public class RouteModel
    {
        /// <summary>
        /// kolom Id Route sebagai Primary key dari tabel Route
        /// </summary>
        public int RouteId { get; set; }

        /// <summary>
        /// Kolom Name memiliki validasi berupa Harus terisi dengan minimum character 3 dam maksimum character 7
        /// </summary>
        [Required]
        [StringLength(7, MinimumLength =3)]
        public string Name { get; set; }

        /// <summary>
        /// Kolom Qty menunjukan jumlah yang dimiliki oleh tabel Route, Memiliki validasi yaitu harus terisi
        /// </summary>
        [Required]
        public string Qty { get; set; }
    }
}
