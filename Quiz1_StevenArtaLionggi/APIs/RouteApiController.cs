﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Quiz1_StevenArtaLionggi.Models;
using Quiz1_StevenArtaLionggi.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Quiz1_StevenArtaLionggi.APIs
{
    [Route("api/v1/route")]
    public class RouteApiController : Controller
    {

        private readonly RouteService _routeServiceMan;

        /// <summary>
        /// Constructor untuk menginisialisasi Service
        /// </summary>
        /// <param name="routeService"></param>
        public RouteApiController(RouteService routeService)
        {
            this._routeServiceMan = routeService;
        }

        /// <summary>
        /// Function untuk mengambil semua Route di database
        /// </summary>
        /// <returns></returns>
        [HttpGet("all-route", Name = "GetAllRoute")]
        public async Task<ActionResult<List<RouteModel>>> GetAllRouteAsync()
        {
            var routes = await this._routeServiceMan.GetAllRouteAsync();

            if(routes == null)
            {
                return BadRequest(null);
            }

            return Ok(routes);
        }

        /// <summary>
        /// Function untuk mengambil data Route yang sudah di filter dengan search query dan di paginate
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="itemPerPage"></param>
        /// <param name="searchKey"></param>
        /// <returns></returns>
        [HttpGet("filter-route", Name = "GetFilteredRoute")]
        public async Task<ActionResult<List<RouteModel>>> GetFilteredRouteAsync([FromQuery]int pageIndex, int itemPerPage, string searchKey)
        {
            var routes = await this._routeServiceMan.GetFilteredRoutes(pageIndex, itemPerPage, searchKey);

            if (routes == null)
            {
                return BadRequest(null);
            }

            return Ok(routes);
        }

        /// <summary>
        /// Function untuk mengambil specific Route berdasarkan routeId
        /// </summary>
        /// <param name="routeId"></param>
        /// <returns></returns>
        [HttpGet("specific-route", Name = "GetSpecificRoute")]
        public async Task<ActionResult<RouteModel>> GetSpecificRouteAsync(int? routeId)
        {
            if(routeId.HasValue == false)
            {
                return BadRequest(null);
            }

            var route = await this._routeServiceMan.GetSpecificRouteAsync(routeId.Value);

            if(route == null)
            {
                return BadRequest(null);
            }

            return Ok(route);
        }

        /// <summary>
        /// Function untuk mendapatkan total data Route
        /// </summary>
        /// <returns></returns>
        [HttpGet("total-data", Name = "GetTotalData")]
        public ActionResult<int> GetTotalData()
        {
            var totalData = this._routeServiceMan.GetTotalData();

            return Ok(totalData);
        }

        /// <summary>
        /// Function untuk menambahkan data pada tabel Route
        /// </summary>
        /// <param name="newRoute"></param>
        /// <returns></returns>
        [HttpPost("insert", Name = "InsertRoute")]
        public async Task<ActionResult<ResponseModel>> InsertRouteAsync([FromBody]RouteModel newRoute)
        {
            var isSuccess = await this._routeServiceMan.InsertRouteAsync(newRoute);

            if(isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Failed to insert new data"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = "Success to insert new data"
            });
        }

        /// <summary>
        /// Function untuk melakukan Update pada data Route 
        /// </summary>
        /// <param name="candidateRoute"></param>
        /// <returns></returns>
        [HttpPut("update", Name = "UpdateRoute")]
        public async Task<ActionResult<ResponseModel>> UpdateRouteAsync([FromBody]RouteModel candidateRoute)
        {
            var isSuccess = await this._routeServiceMan.UpdateRouteAsync(candidateRoute);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "ID Not Found!!!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success to Update data {candidateRoute.Name}"
            });
        }

        /// <summary>
        /// Function untuk menghapus record pada tabel Route
        /// </summary>
        /// <param name="routeId"></param>
        /// <returns></returns>
        [HttpDelete("delete/{routeId}", Name = "DeleteRoute")]
        public async Task<ActionResult<ResponseModel>> DeleteRouteAsync(int routeId)
        {
            var isSuccess = await this._routeServiceMan.DeleteRouteAsync(routeId);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "ID Not Found!!!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success to Delete route with ID {routeId}"
            });
        }
    }
}
