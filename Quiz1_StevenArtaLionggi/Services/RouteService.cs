﻿using Microsoft.EntityFrameworkCore;
using Quiz1_StevenArtaLionggi.entities;
using Quiz1_StevenArtaLionggi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_StevenArtaLionggi.Services
{
    public class RouteService
    {
        private readonly MasterDbContext _dbContext;

        /// <summary>
        /// Constructor untuk menginisialisasi DbContext
        /// </summary>
        /// <param name="masterDbContext"></param>
        public RouteService(MasterDbContext masterDbContext)
        {
            this._dbContext = masterDbContext;
        }

        /// <summary>
        /// Function untuk mengambil semua data Route pada database secara asynchronus
        /// </summary>
        /// <returns></returns>
        public async Task<List<RouteModel>> GetAllRouteAsync()
        {
            var routes = await this._dbContext
                .Routes
                .Select(Q => new RouteModel
                {
                    RouteId = Q.RouteId,
                    Name = Q.Name,
                    Qty = Q.Qty
                })
                .AsNoTracking()
                .ToListAsync();

            return routes;
        }

        /// <summary>
        /// Function untuk menambahkan data pada tabel Route
        /// </summary>
        /// <param name="newRoute"></param>
        /// <returns></returns>
        public async Task<bool> InsertRouteAsync(RouteModel newRoute)
        {
            this._dbContext.Add(new Route
            {
                Name = newRoute.Name,
                Qty = newRoute.Qty
            });

            await this._dbContext.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Function untuk melakukan update pada tabel Route
        /// </summary>
        /// <param name="route"></param>
        /// <returns></returns>
        public async Task<bool> UpdateRouteAsync(RouteModel route)
        {
            var candidate = await this._dbContext
                .Routes
                .Where(Q => Q.RouteId == route.RouteId)
                .FirstOrDefaultAsync();

            if (candidate == null)
            {
                return false;
            }

            candidate.Name = route.Name;
            candidate.Qty = route.Qty;

            await this._dbContext.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Function untuk menghapus data Route dari database
        /// </summary>
        /// <param name="routeId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteRouteAsync(int routeId)
        {
            var candidate = await this._dbContext
                .Routes
                .Where(Q => Q.RouteId == routeId)
                .FirstOrDefaultAsync();

            if(candidate == null)
            {
                return false;
            }

            this._dbContext.Remove(candidate);
            await this._dbContext.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Function untuk mengambil data spesifik berdasarkan Id Router
        /// </summary>
        /// <param name="routeId"></param>
        /// <returns></returns>
        public async Task<RouteModel> GetSpecificRouteAsync(int routeId)
        {
            var route = await this._dbContext
                .Routes
                .Where(Q => Q.RouteId == routeId)
                .Select(Q => new RouteModel
                {
                    RouteId = Q.RouteId,
                    Name = Q.Name,
                    Qty = Q.Qty
                })
                .AsNoTracking()
                .FirstOrDefaultAsync();

            return route;
        }

        /// <summary>
        /// Function untuk mengambil data dengan pagination dan query Search
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="itemPerPage"></param>
        /// <param name="searchKey"></param>
        /// <returns></returns>
        public async Task<List<RouteModel>> GetFilteredRoutes(int pageIndex, int itemPerPage, string searchKey)
        {
            var query = this._dbContext
                .Routes
                .AsQueryable();

            if(string.IsNullOrEmpty(searchKey) == false)
            {
                query = query
                    .Where(Q => Q.Name.StartsWith(searchKey));
            }

            var route = await query
                .Select(Q => new RouteModel
                {
                    RouteId = Q.RouteId,
                    Name = Q.Name,
                    Qty = Q.Qty
                })
                .Skip((pageIndex - 1) * itemPerPage)
                .Take(itemPerPage)
                .AsNoTracking()
                .ToListAsync();

            return route;
        }

        /// <summary>
        /// Function untuk mengambil banyak data
        /// </summary>
        /// <returns></returns>
        public int GetTotalData()
        {
            var totalRoute = this._dbContext
                .Routes
                .Count();

            return totalRoute;
        }

    }
}
